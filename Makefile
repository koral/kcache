SRCS = test_top.v sram.v kcache.v kcache_defs.v
IFLAGS = -Wall -Wselect-range -Wportbind -Wfloating-nets

default: all

seq: $(SRCS) seq.test
	iverilog $(IFLAGS) -o seq $(SRCS) -DTEST_FILE="\"seq.test\""
	vvp seq

rnd: $(SRCS) rnd.test
	iverilog $(IFLAGS) -o rnd $(SRCS) -DTEST_FILE="\"rnd.test\""
	vvp rnd

seq_rnd: $(SRCS) seq_rnd.test
	iverilog $(IFLAGS) -o seq_rnd $(SRCS) -DTEST_FILE="\"seq_rnd.test\""
	vvp seq_rnd

all: seq rnd seq_rnd


clean:
	rm -f seq_rnd rnd seq kcache.vcd
