`timescale 1ns / 1ps
//-----------------------------------------------------------------------------
// Title	 : KPU cache main file
// Project	 : KPU
//-----------------------------------------------------------------------------
// File		 : kcache.v
// Author	 : acorallo  <andrea_corallo@yahoo.it>
// Created	 : 6.12.2017
//-----------------------------------------------------------------------------
// Description :
// Main file for KPU
//-----------------------------------------------------------------------------
// This file is part of KPU.
// KPU is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// KPU is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY;
// without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with KPU.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright (c) 2017 by Andrea Corallo.
//------------------------------------------------------------------------------
// Modification history :
// 6.12.2017 : created
//-----------------------------------------------------------------------------

/*
 | cache state | line state |
 |-------------+------------|
 | IDLE        | VALID      |
 | LOADING     | DIRTY      |
 | STORING     | INVALID    |

 cache states async updated
 line states are sync updated

 status diagram:

                        +--------------------+
                        |                    |
              +-------> |  LOADING INVALID   |
              |         |                    |
              |         +---------+----------+
              |                   |
              |addr mismatch      |load completed
              |                   |
        +-----+--------+          |
        |              | <--------+
        |  IDLE VALID  |
        |              | <--------+
        +-----+--------+          |
              |                   |
              |data write         |
              |                   |
              v                   |
        +--------------+          |store completed
        |              |          |line addr is set to -1
        | IDLE DIRTY   |          |
        |              |          |
        +-----+--------+          |
              |                   |
              |addr mismatch ||   |
              |flush req          |
              |                   |
              |         +---------+---------+
              |         |                   |
              +------>  | STORING INVALID   |
                        |                   |
                        +-------------------+

*/

`include	"kcache_defs.v"

module kcache(input wire		    clk_i,
	      output reg 		    wait_o, // Stop kpu execution if needed
	      input wire 		    flush_i, // flush data cache and invalidate inst cache
	      input wire [`N-1:0] 	    i_addr_i, // inst memory address
	      output wire [`N-1:0] 	    i_data_o, // inst output
	      input wire [`N-1:0] 	    d_addr_i, // data memory address
	      input wire [3:0] 		    d_sel_wr_i, // data byte select
	      input wire [`N-1:0] 	    d_data_i, // data input
	      output wire [`N-1:0] 	    d_data_o, // data output
	      // SRAM interface
	      output reg [`SRAM_ADDR_W-1:0] sram_addr_o,
	      inout wire [`SRAM_DATA_W-1:0] sram_data_io,
	      output reg 		    sram_cs_o,
	      output reg 		    sram_we_o,
	      output reg 		    sram_oe_o,
	      output reg 		    sram_hb_o,
	      output reg 		    sram_lb_o
	      );

	reg [`SRAM_DATA_W-1:0] 	sram_data_write;


	reg [`N-1:0] 		i_cache_line [0:`CACHE_LINE_SIZE - 1] /* synthesis syn_ramstyle=no_rw_check */;
	reg [`N-1:0] 		d_cache_line [0:`CACHE_LINE_SIZE - 1] /* synthesis syn_ramstyle=no_rw_check */;
	reg [`PAGES_N_BITS :0] 	i_page_num; // we reserve 1 bit more to be used as invalid
	reg [`PAGES_N_BITS :0] 	d_page_num; // we reserve 1 bit more to be used as invalid
	localparam		INVALID_PAGE_NUM = (1 << `PAGES_N_BITS) - 1;

	localparam		CACHE_IDLE = 2'h0;
	localparam		CACHE_LOADING = 2'h1;
	localparam		CACHE_STORING  = 2'h2;
	reg [2:0] 		i_cache_state  = CACHE_IDLE;
	reg [2:0] 		d_cache_state  = CACHE_IDLE;

	localparam		LINE_VALID    = 0; // data are valid and coherent with ram
	localparam		LINE_DIRTY    = 1; // data are valid but ram has to be updated
	localparam		LINE_INVALID  = 2; // data are invalid and have to be loaded
	reg [1:0] 		i_line_state  = LINE_VALID;
	reg [1:0]		d_line_state  = LINE_VALID;
	reg [`SRAM_ADDR_W - 1 : 0] i_line_base_addr_orig;
	reg [`SRAM_ADDR_W - 1 : 0] d_line_base_addr_orig;

	wire [`CACHE_LINE_BITS - 1:0] 	i_line_addr;
	wire [`CACHE_LINE_BITS - 1:0] 	d_line_addr;

	reg [`CACHE_LINE_BITS + 1:0] 		cycle_num;

	// given an addr return the page num
	function [`PAGES_N_BITS - 1 : 0] f_page_num;
		input [`N-1 :0] a;
		f_page_num = a >> (`PAGES_N_BITS + 1); // f_page_num = a / PAGE_SIZE;
	endfunction

	// initial addr of the page
	function [`SRAM_ADDR_W - 1 : 0] f_base_sram_addr;
		input [`N-1 :0] a;
		f_base_sram_addr = f_page_num(a) << (`PAGES_N_BITS);
	endfunction // f_base_sram_addr

	assign i_line_addr  = i_addr_i >> 2;
	assign d_line_addr  = d_addr_i >> 2;

	assign sram_data_io  = (!sram_cs_o && !sram_oe_o) ? `SRAM_DATA_W'bz : sram_data_write;

	assign i_data_o  = i_cache_line[i_line_addr];
	assign d_data_o  = d_cache_line[d_line_addr];

	initial
		begin
			cycle_num   = -1;
			i_page_num  = INVALID_PAGE_NUM;
			d_page_num  = INVALID_PAGE_NUM;
			sram_hb_o   = 1;
			sram_lb_o   = 1;
		end

	always @(posedge clk_i) begin

		if (i_cache_state != CACHE_IDLE ||
		    d_cache_state != CACHE_IDLE)
			wait_o 	  <= #1 1'b1;
		else
			wait_o    <= #1 1'b0;

		if (flush_i) begin
			cycle_num   <= #1 -1;
			i_page_num  <= #1 INVALID_PAGE_NUM;
			d_page_num  <= #1 INVALID_PAGE_NUM;
		end
		else begin

			//////////////////////////////////////////////
			// Instruction cache main sync logic	    //
			//////////////////////////////////////////////
			case (i_cache_state)
				CACHE_IDLE: begin
					i_line_base_addr_orig <= #1 f_base_sram_addr(i_addr_i);
				end
				CACHE_LOADING: begin
					sram_cs_o        <= #1 1'b0;
					sram_oe_o        <= #1 1'b0;

					if (cycle_num == `CACHE_LINE_SIZE << 1) begin
						i_line_state <= #1 LINE_VALID;
						i_page_num   <= #1 f_page_num(d_addr_i);
						cycle_num    <= #1 -1;
					end
					else begin
						i_page_num <= #1 INVALID_PAGE_NUM;
						cycle_num  <= #1 cycle_num + 1;
						if (cycle_num[0])
							i_cache_line[cycle_num >> 1][31:16] <= #1 sram_data_io;
						else
							i_cache_line[cycle_num >> 1][15:0] <= #1 sram_data_io;
						if (`KCACHE_VERBOSE &&
						    sram_data_io !== `SRAM_DATA_W'hxxxx)
							$display("loading into i cache at %x %x cycl %x", cycle_num >> 1,  sram_data_io, cycle_num);
					end
				end
			endcase // case i_cache_state

			///////////////////////////////////////
			// Data cache main sync logic	     //
			///////////////////////////////////////
			case (d_cache_state)
				CACHE_IDLE: begin
					sram_cs_o <= #1 1'b1;
					sram_oe_o <= #1 1'b1;

					if (d_sel_wr_i[0])
						d_cache_line[d_line_addr][7:0] <= #1 d_data_i[7:0];
					if (d_sel_wr_i[1])
						d_cache_line[d_line_addr][15:8] <= #1 d_data_i[15:8];
					if (d_sel_wr_i[2])
						d_cache_line[d_line_addr][23:16] <= #1 d_data_i[23:16];
					if (d_sel_wr_i[3])
						d_cache_line[d_line_addr][31:24] <= #1 d_data_i[31:24];
					if (d_sel_wr_i != 4'b0)
						d_line_state  <= #1 LINE_DIRTY;
					d_line_base_addr_orig <= #1 f_base_sram_addr(d_addr_i);

					if (`KCACHE_VERBOSE && d_sel_wr_i != 0)
						$display ("storing into cache at 0x%x 0x%x 0x%x", d_line_addr, d_data_i, d_sel_wr_i);

				end
				CACHE_LOADING: begin
					sram_cs_o        <= #1 1'b0;
					sram_oe_o        <= #1 1'b0;

					if (cycle_num == `CACHE_LINE_SIZE << 1) begin
						d_line_state <= #1 LINE_VALID;
						d_page_num   <= #1 f_page_num(d_addr_i);
						cycle_num    <= #1 -1;
					end
					else begin
						d_page_num <= #1 INVALID_PAGE_NUM;
						cycle_num  <= #1 cycle_num + 1;
						if (cycle_num[0])
							d_cache_line[cycle_num >> 1][31:16] <= #1 sram_data_io;
						else
							d_cache_line[cycle_num >> 1][15:0] <= #1 sram_data_io;
						if (`KCACHE_VERBOSE &&
						    sram_data_io !== `SRAM_DATA_W'hxxxx)
							$display("loading into d cache at %x %x cycl %x", cycle_num >> 1,  sram_data_io, cycle_num);

					end
				end
				CACHE_STORING: begin
					sram_cs_o 	  <= #1 1'b0;
					sram_oe_o 	  <= #1 1'b1;


					if (cycle_num == (`CACHE_LINE_SIZE << 1) - 1) begin
						d_line_state <= #1 LINE_INVALID;
						d_page_num   <= #1 f_page_num(d_addr_i);
						cycle_num    <= #1 -1;
					end
					else begin
						d_page_num <= #1 INVALID_PAGE_NUM;
						cycle_num  <= #1 cycle_num + 1;
					end
				end
			endcase // case d_cache_state
		end

	end // always @ (posedge clk_i)

	always @(clk_i) begin

		if (d_cache_state == CACHE_STORING && cycle_num != 11'h7ff)
			sram_we_o  <= #1 ~clk_i;
		else
			sram_we_o  <= #1 1'b1;
	end

	///////////////////////////////////
	// async logic driving sram_addr //
	///////////////////////////////////

	always @(d_cache_line[cycle_num >> 1] or i_addr_i or d_addr_i or
		 i_cache_state or d_cache_state or d_line_base_addr_orig or cycle_num) begin

		// Latch prevention
		sram_addr_o  = 0;
		sram_data_write = 0;

		if (i_cache_state == CACHE_LOADING)
			sram_addr_o = f_base_sram_addr(i_addr_i) | cycle_num;

		case (d_cache_state)
			CACHE_LOADING: begin
				sram_addr_o = f_base_sram_addr(d_addr_i) | cycle_num;
			end
			CACHE_STORING: begin
				sram_addr_o  = d_line_base_addr_orig | cycle_num;
				if (cycle_num[0])
					sram_data_write = d_cache_line[cycle_num >> 1][31:16];
				else
					sram_data_write = d_cache_line[cycle_num >> 1][15:0];
			end
		endcase // case (d_cache_state)

	end

	always @(*) begin

		case (d_line_state)
			LINE_VALID: begin
				if (f_page_num(d_addr_i)  == d_page_num ||
				    i_cache_state     != CACHE_IDLE)
					// whatever instcache is already doing we wait for it
					d_cache_state  = CACHE_IDLE;
				else
					d_cache_state  = CACHE_LOADING;
			end
			LINE_DIRTY: begin
				if (f_page_num(d_addr_i)  == d_page_num ||
				    i_cache_state != CACHE_IDLE)
					// whatever instcache is already doing we wait for it
					d_cache_state  = CACHE_IDLE;
				else
					d_cache_state  = CACHE_STORING;
			end
			LINE_INVALID: begin
				if (i_cache_state != CACHE_IDLE)
					// whatever instcache is already doing we wait for it
					d_cache_state  = CACHE_IDLE;
				else
					d_cache_state  = CACHE_LOADING;
			end
		endcase // case (d_line_state)

		// We gave precedence to data cache status transition
		case (i_line_state)
			LINE_VALID: begin
				if (f_page_num(i_addr_i)  == i_page_num ||
				    d_cache_state     != CACHE_IDLE)
					// whatever datacache is already doing we wait for it
					i_cache_state  = CACHE_IDLE;
				else
					i_cache_state  = CACHE_LOADING;
			end
			LINE_INVALID: begin
				if (d_cache_state != CACHE_IDLE)
					// whatever datacache is already doing we wait for it
					i_cache_state  = CACHE_IDLE;
				else
					i_cache_state  = CACHE_LOADING;
			end
		endcase // case (i_line_state)

	end // always @ (*)

endmodule
