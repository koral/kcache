`timescale 1ns / 1ps
`include	"kcache_defs.v"

module top_kcache_test();
	reg			clk;
	integer			t;
	reg 			flush;
	reg [`N-1:0] 		data_send;
	wire [`N-1:0]		data_rcv;
	wire [`N-1:0]		inst_rcv;
	reg [`N-1:0] 		i_addr;
	reg [`N-1:0]		d_addr;
	reg [3:0] 		d_sel_wr;
	wire			wait_cache;
	reg 			verifying = 0;

        integer			ch_read  = 0;
	integer 		fd_in;

	wire			sram_cs, sram_we, sram_oe, sram_hb, sram_lb;
	wire [`SRAM_DATA_W-1:0] sram_data_io;
	wire [`SRAM_ADDR_W-1:0]	sram_addr;



	parameter STDIN  = 32'h8000_0001;

	reg [`N-1:0] 	test_mem [(1 << `SRAM_ADDR_W) - 1:0];

	task cache_flush;
		begin
			flush  = 1;
			#10;
			flush  = 0;
		end
	endtask

	always #5 clk <= !clk;

	sram sram_i(.addr_i(sram_addr),
		    .data_io(sram_data_io),
		    .CS_i(sram_cs),
		    .WE_i(sram_we),
		    .OE_i(sram_oe),
		    .HB_i(1'b1),
		    .LB_i(1'b1)
		    );

	initial
		begin

			if (`KCACHE_DUMP_VARS) begin
				$dumpfile("kcache.vcd");
				$dumpvars;
			end

			clk 	 = 0;
			t 	 = 0;
			ch_read  = 0;
			i_addr 	 = 0;
			d_addr 	 = 0;
			flush 	 = 0;

			$display("kcache test intiated");
			fd_in  = $fopen(`TEST_FILE, "r");

			while ($fgetc(fd_in) != "\n")
				#0;
			#1;
			while (ch_read != -1) begin
				if (!wait_cache) begin
					ch_read  = $fscanf(fd_in, "%x", d_addr);
					ch_read  = $fscanf(fd_in, "%x", data_send);
					ch_read  = $fscanf(fd_in, "%x", d_sel_wr);
					if (d_sel_wr[0])
						test_mem[d_addr>>2][7:0] = data_send[7:0];
					if (d_sel_wr[1])
						test_mem[d_addr>>2][15:8] = data_send[15:8];
					if (d_sel_wr[2])
						test_mem[d_addr>>2][23:16] = data_send[23:16];
					if (d_sel_wr[3])
						test_mem[d_addr>>2][31:24] = data_send[31:24];
					if (`KCACHE_VERBOSE)
						$display("0x%x\t0x%x\t0x%x\t", d_addr, data_send, d_sel_wr);
				end else
					begin
						// $display("Waiting");
						#10;
					end
				#10;
			end // while (ch_read != -1)

			$display("Injection completed. Verifying...");
			cache_flush();
			verifying    = 1;
			d_sel_wr     = 4'b0;

			for (d_addr = 0; d_addr < (1 << `SRAM_ADDR_W); d_addr = d_addr + 4 ) begin
				i_addr = d_addr;
				#10;
				while(wait_cache)
					#1;
				if (test_mem[d_addr>>2] !== data_rcv) begin
					$display("Error data mismatch for data addr 0x%x", d_addr);
					$display("Orig data 0x%x", test_mem[d_addr>>2]);
					$display("Retrived data 0x%x", data_rcv);
					$error;
					$finish;
				end
			end // for (d_addr = 0; d_addr < (1 << `SRAM_ADDR_W); d_addr = d_addr + 4 )
			$display("Test passed");
			$finish;
		end

	always @(posedge clk) begin
		t  = t + 1;
	end

	kcache kcache_i(.clk_i(clk),
			.wait_o(wait_cache),
			.flush_i(flush),
			.i_addr_i(i_addr),
			.i_data_o(inst_rcv),
			.d_addr_i(d_addr),
			.d_sel_wr_i(d_sel_wr),
			.d_data_i(data_send),
			.d_data_o(data_rcv),
			.sram_addr_o(sram_addr),
			.sram_data_io(sram_data_io),
			.sram_cs_o(sram_cs),
			.sram_we_o(sram_we),
			.sram_oe_o(sram_oe),
			.sram_hb_o(sram_hb),
			.sram_lb_o(sram_lb));

endmodule // top_kcache_test
