`timescale 1ns / 1ps
/////////////////////////////////////////////////
// SRAM Model				       //
// IS62WV51216ALL, IS62WV51216BLL	       //
// http://www.issi.com/WW/pdf/62WV51216ALL.pdf //
/////////////////////////////////////////////////
`include	"kcache_defs.v"

module sram (input [`SRAM_ADDR_W-1:0] addr_i,
	     inout [`SRAM_DATA_W-1:0] data_io,
	     input CS_i,
	     input WE_i,
	     input OE_i,
	     input HB_i,	// Fake
	     input LB_i);	// Fake

	reg [`SRAM_DATA_W-1:0] 	mem [(1 << `SRAM_ADDR_W) - 1:0];

	assign data_io = (!CS_i && !OE_i) ? mem[addr_i] : `SRAM_DATA_W'bz;

	always @(posedge WE_i)
		if (!CS_i)
			mem[addr_i]  = data_io;

	always @(posedge WE_i)
		if (`KCACHE_VERBOSE &&
		    data_io !== `SRAM_DATA_W'hxxxx)
			$display ("storing into SRAM at 0x%x (0x%x) 0x%x", addr_i, addr_i << 1, data_io);

	always @(WE_i or OE_i)
		if (!WE_i && !OE_i)
			$display("Operational error in SRAM: OE and WE both active");

endmodule
