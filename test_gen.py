import random

#######################################################################################
# Generate a simulus that fills sequentially the ram + does some random writes on top #
#######################################################################################

MEGABYTE = 1024 * 1024
RAM_SIZE = MEGABYTE
RANDOM_WRITES = 1000

SEQ = True
RAND = True

print "#addr		data		byte_select"

if SEQ:
    for i in range (RAM_SIZE >> 2):
        print "0x" + format(i << 2, '08x') + "\t0x" + \
            format(random.randint(0, 0xffffffff), '08x') + "\t0xf"
if RAND:
        for i in range (RANDOM_WRITES):
            byte_sel = random.randint(1, 0xf)
            print "0x" + format(random.randint(0, RAM_SIZE >> 2) << 2, '08x') + "\t0x" + \
                format(random.randint(0, 0xffffffff), '08x') + "\t0x" + \
                format(byte_sel, '01x')
