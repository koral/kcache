`ifndef _kcache_defs
 `define _kcache_defs

`define N			32
`define EXT_SRAM_SIZE		(1024 * 1024)		// 1MB
`define PAGE_SIZE		(4 * 1024)		// 4KB
`define PAGES_N_BITS		11			// We need 11 bits to address 1024 pages
`define CACHE_LINE_BITS		10
`define CACHE_LINE_SIZE		(1 << `CACHE_LINE_BITS)	// 1K words => 4KB

`define SRAM_DATA_W		16
`define SRAM_ADDR_W		19

`define KCACHE_VERBOSE		0
`define KCACHE_DUMP_VARS	1

`endif //  `ifndef _kcache_defs
